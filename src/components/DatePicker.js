import React from 'react'

const THE_YEARS = new Array(100).fill(0).map((_, idx) => 2017 - idx)

export const YearPicker = ({ onChange = () => {}, ...props }) => {
  const options = THE_YEARS.map(year => <option key={year} value={year}>{year}</option>)

  return (
    <select onChange={evt => onChange(evt.target.value)} {...props}>
      {options}
    </select>
  )
}

export const DatePicker = ({ value, onChange }) => {
  return (
    <YearPicker value={value} onChange={onChange}/>
  )
}
