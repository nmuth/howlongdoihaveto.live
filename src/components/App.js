import React, { useRef, useState } from 'react'
import { useInterval } from '../hooks'
import * as actuary from '../services/actuary'
import { YearPicker } from './DatePicker'
import { GenderPicker } from './GenderPicker'
import { RacePicker } from './RacePicker'
import './App.css'

function App() {
  const [birthYear, setBirthYear] = useState(1970)
  const [gender, setGender] = useState()
  const [race, setRace] = useState()
  const [displayResults, setDisplayResults] = useState(false)
  const birthDate = new Date(`${birthYear}-01-01`)
  const age = new Date().getFullYear() - birthDate.getFullYear()

  const data = actuary.getDataForCriteria({ age, race, gender })

  const startTime = useRef(new Date())
  const deathYear = useRef(new Date())
  deathYear.current.setYear(startTime.current.getFullYear() + data.lifeExpectancy)

  const [totalMsLeft, setTotalMsLeft] = useState(deathYear.current - startTime.current)

  useInterval(() => {
    setTotalMsLeft(deathYear.current - Date.now())
  }, 500)

  const totalSecondsLeft = totalMsLeft / 1000
  const totalMinutesLeft = totalSecondsLeft / 60
  const totalHoursLeft = totalMinutesLeft / 60
  const totalDaysLeft = totalHoursLeft / 24
  const totalYearsLeft = totalDaysLeft / 365.25

  const yearsLeft = Math.floor(totalYearsLeft)
  const daysLeft = Math.floor(totalDaysLeft % 365)
  const hoursLeft = Math.floor(totalHoursLeft % 24)
  const minutesLeft = Math.floor(totalMinutesLeft % 60)
  const secondsLeft = Math.floor(totalSecondsLeft % 60)

  const timeLeft = `${yearsLeft} years, ${daysLeft} days, ${hoursLeft} hours, ${minutesLeft} minutes, ${secondsLeft} seconds`

  return (
    <div className="App">
      <main>
        <header>
          <h1>how long do I have to live?</h1>
        </header>
        {
          !displayResults && (
            <div className="App__pickers">
              <label htmlFor="App__GenderPicker">gender</label>
              <GenderPicker id="App__GenderPicker" value={gender} onChange={setGender}/>
              <label htmlFor="App__RacePicker">ethnicity</label>
              <RacePicker id="App__RacePicker" value={race} onChange={setRace}/>
              <label htmlFor="App__YearPicker">birth year</label>
              <YearPicker id="App__YearPicker" value={birthYear} onChange={setBirthYear}/>
            </div>
          )
        }
        <div className="App__results">
          {
            displayResults
            ? (
              <>
                <p className="App__results__timeLeft">{timeLeft}</p>
                <p>you will live for {data.lifeExpectancy.toFixed(2)} more years</p>
                <p>you will die in the year {deathYear.current.getFullYear()} at age {deathYear.current.getFullYear() - birthYear}</p>
                <p>your chance of dying this year: {(data.chanceOfDeathThisYear * 100).toFixed(2)}%</p>
                <button onClick={() => setDisplayResults(false)}>do it again</button>
              </>
            ) : (
              <button onClick={() => setDisplayResults(true)}>show results</button>
            )
          }
        </div>
      </main>
      <footer>
        © Noah Muth {new Date().getFullYear()}
        &nbsp;· served by <a href="https://netlify.com" target="_blank">netlify</a>
        &nbsp;· data from <a href="https://www.cdc.gov/nchs/data/nvsr/nvsr68/nvsr68_07-508.pdf" target="_blank">CDC</a>
      </footer>
    </div>
  );
}

export default App;
