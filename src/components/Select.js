import React from 'react'

export const Select = ({ onChange = () => {}, options = [], ...props }) => (
  <select onChange={evt => onChange(evt.target.value)} {...props}>
    {options.map(({ value, label }) => (
      <option key={label || value} value={value}>
        {label || value}
      </option>
    ))}
  </select>
)
