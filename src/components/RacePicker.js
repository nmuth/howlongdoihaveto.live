import React from 'react'
import { Select } from './Select'

const RACE_OPTIONS = [
  { value: undefined, label: 'unspecified' },
  { value: 'nonhispanicwhite', label: 'white / caucasian' },
  { value: 'white', label: 'white / hispanic' },
  { value: 'nonhispanicblack', label: 'black / african-american' },
  { value: 'black', label: 'black / hispanic' },
  { value: 'hispanic', label: 'hispanic' },
  { value: 'all', label: 'other' },
]

export const RacePicker = ({ ...props }) => (
  <Select options={RACE_OPTIONS} {...props}/>
)
