import React from 'react'
import { Select } from './Select'

const GENDER_OPTIONS = [
  { label: 'unspecified', value: undefined },
  { value: 'male' },
  { value: 'female' },
  { label: 'nonbinary', value: 'all' },
]

export const GenderPicker = ({ onChange = () => {}, ...props }) => (
  <Select options={GENDER_OPTIONS} {...props}/>
)
