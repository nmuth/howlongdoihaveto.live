import find from 'lodash/find'
import tables from './actuary-data.json'

export function getDataForCriteria({ gender = 'all', race = 'all', age = 0 } = {}) {
  const table = find(tables, (val, key) => {
    const [, tableRace, tableGender] = key.split('-')
    return tableRace === race && tableGender === gender
  })

  if (!table) {
    throw new Error(`no table defined for ${race}-${gender}`)
  }

  return table[age]
}

export function getDataForBirthDate(table, age) {
  if (age < 0) {
    age = 0
  } else if (age > 100) {
    age = 100
  }

  return table[age]
}
