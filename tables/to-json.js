const fs = require('fs')
const { promisify } = require('util')
const convertExcelToJson = require('convert-excel-to-json')

const readdir = promisify(fs.readdir)
const writeFile = promisify(fs.writeFile)

const BASE_CONFIG = {
  range: 'A4:G104',
  columnToKey: {
    A: 'age',
    B: 'chanceOfDeathThisYear',
    C: 'numberSurviveToThisAge',
    D: 'numberDieThisYear',
    E: 'personYearsLivedThisYear',
    F: 'personYearsLivedAfterThisYear',
    G: 'lifeExpectancy'
  }
}

async function doIt() {
  const startTime = new Date()
  const excelFiles = (await readdir('.')).filter(filename => filename.endsWith('.xlsx'))
  const data = {}

  for (const filename of excelFiles) {
    console.info(`reading ${filename}...`)

    const key = filename.replace('.xlsx', '')
    const fileData = convertExcelToJson({
      ...BASE_CONFIG,
      sourceFile: filename,
    })
    const [tableData] = Object.values(fileData)

    data[key] = tableData.map(row => {
      const [startAge] = row.age.split('-')

      return {
        ...row,
        age: parseInt(startAge),
      }
    })
  }

  await writeFile('../src/services/actuary-data.json', JSON.stringify(data, null, '  '))
  console.info('wrote ../src/services/actuary-data.json')
  console.info('done in', new Date() - startTime, 'ms')
}

doIt()
